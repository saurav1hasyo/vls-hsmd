#!/bin/bash

# Check if an argument is provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 TEST_DIR"
    exit 1
fi

# Assign TEST_DIR from the argument
TEST_DIR=$1

# Define the expected files and directories
declare -a expected=("errlog" "bitcoin.conf" "log" "lss" "regtest")

# Initialize counters
count_processed=0
count_deleted=0
session_deleted=0

check_and_delete() {
    local dir="$1"
    local unexpected_found=0
    ((count_processed++))

    # Iterate over the contents of the directory
    for item in "$dir"/*; do

        # WORKAROUND - prune reckless side files, not needed for debugging
        rm -f "${dir}/rkls_api_lightningd_plugins.json"
        rm -f "${dir}/.gitconfig"
        rm -rf "${dir}/lightningd/testplugpyproj"
        rm -rf "${dir}/lightningd/testplugfail"
        rm -rf "${dir}/lightningd/testplugpass"
        rm -rf "${dir}/lightningd/plugins"
        rm -rf "${dir}/lightningd/scratch-index"

        # Prune empty directories, fails if not dir or not empty
        rmdir "$item" 2>/dev/null || true

        if [ -f "$item" ] || [ -d "$item" ]; then
            if [[ ! " ${expected[*]} " =~ " $(basename "$item") " ]]; then
                unexpected_found=1
                break
            fi
        fi
    done

    # If no unexpected file or directory is found, delete the directory
    if (( unexpected_found == 0 )); then
        rm -rf "$dir"
        ((count_deleted++))
    fi
}

# Main loop to iterate over two-deep subdirectories of TEST_DIR
for subdir in "$TEST_DIR"/*/*; do
    if [ -d "$subdir" ]; then
        check_and_delete "$subdir"
    fi
done

# Check if first-level subdirectories are empty and remove them
for subdir in "$TEST_DIR"/*; do
    if [ -d "$subdir" ] && [ -z "$(ls -A "$subdir")" ]; then
        rm -rf "$subdir"
        ((session_deleted++))
    fi
done

# Print summary
echo "Checked $count_processed test directories. Deleted $count_deleted test directories and $session_deleted session directories."
