stages:
  - build
  - test

cache:
  paths:
    - .cache

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  CARGO_HOME: "$CI_PROJECT_DIR/.cache/cargo"
  TEST_DIR: "$CI_BUILDS_DIR/TESTS"
  SUBDAEMON:
    value: "hsmd:remote_hsmd_socket"
    description: "use the socket based hsmd replacement procy (default)"
  LIGHTNING_REF:
    value: ""
    description: "rebase onto the ref (e.g. master) from the upstream c-lightning repository"

get-bitcoin:
  stage: build
  image: python:3.8
  cache:
    key: vls-bitcoin
    paths:
      - bin
  script:
    - BITCOIN_VERSION=24.0.1
    - mkdir -p bin
    - ls -l bin
    - if [ ! -e bin/bitcoind ]; then
        wget https://bitcoincore.org/bin/bitcoin-core-${BITCOIN_VERSION}/bitcoin-${BITCOIN_VERSION}-x86_64-linux-gnu.tar.gz &&
        tar -xf bitcoin-$BITCOIN_VERSION-x86_64-linux-gnu.tar.gz &&
        mv bitcoin-$BITCOIN_VERSION/bin/* bin ;
      fi
  artifacts:
    paths:
      - bin/bitcoind
      - bin/bitcoin-cli
    expire_in: 1 week

compile-vls:
  stage: build
  tags: [ saas-linux-large-amd64 ]
  image: rust:1.67.0
  timeout: 30m
  script:
    # we don't need to rebase lightning here, but fail fast if rebase fails
    # - ./scripts/maybe-rebase "$LIGHTNING_REF"
    # latest rust images seem to require rustfmt for cargo build
    - apt-get update
    - apt-get install -y protobuf-compiler
    - rustup component add rustfmt
    - make setup
    - make list-versions
    - (cd vls && git status)
    - (cd vls && git clean -f && cargo clean)
    - (cd vls/lightning-storage-server && git clean -f && cargo clean)
    - (cd vls && git status)
    - (cd vls && cargo build --locked --bins)
    - (cd vls/lightning-storage-server && cargo build --locked --bins)
    # replace the symbolic links w/ actual files
    - mkdir -p newbin && cp -L bin/* newbin/ && rm -rf bin && mv newbin bin
    - cp vls/contrib/howto/assets/log{elide,filter,sum,testdir} bin/
    - ls -l bin
    - bin/remote_hsmd_socket --git-desc
  artifacts:
    paths:
      - bin/vlsd2
      - bin/lssd
      - bin/remote_hsmd_socket
      - bin/remote_hsmd_serial
      - bin/logelide
      - bin/logfilter
      - bin/logsum
      - bin/logtestdir
    expire_in: 1 week

compile-cln:
  stage: build
  tags: [ saas-linux-large-amd64 ]
  image: ubuntu
  script:
    - ./scripts/install-additional
    - git submodule update --init --recursive
    - make list-versions
    - cd lightning
    - bash -x .github/scripts/setup.sh
    - bash -x ../scripts/ci-build.sh
    - zip ../bin-cln.zip $(find lightningd cli plugins tools devtools tests/plugins -executable -type f) config.vars
  artifacts:
    paths:
      - bin-cln.zip
    expire_in: 1 week

test-openv1-enforcing-socket:
  stage: test
  tags: [ saas-linux-large-amd64 ]
  image: ubuntu
  variables:
  script:
    - ./scripts/install-additional
    - git submodule update --init --recursive
    - make list-versions
    - ./scripts/setup-remote-hsmd
    - source scripts/setup-env
    - remote_hsmd_socket --git-desc
    - vlsd2 --git-desc
    - cd lightning
    - unzip -o ../bin-cln.zip
    - bash -x ../scripts/ci-test.sh
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/TESTS/
    exclude:
      - ${CI_PROJECT_DIR}/TESTS/*/*/*/*/lightning-rpc
    expire_in: 1 week

test-openv1-permissive-socket:
  stage: test
  tags: [ saas-linux-large-amd64 ]
  image: ubuntu
  variables:
    VLS_PERMISSIVE: "1"
  script:
    - ./scripts/install-additional
    - git submodule update --init --recursive
    - make list-versions
    - ./scripts/setup-remote-hsmd
    - source scripts/setup-env
    - remote_hsmd_socket --git-desc
    - cd lightning
    - unzip -o ../bin-cln.zip
    - bash -x ../scripts/ci-test.sh
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/TESTS/
    exclude:
      - ${CI_PROJECT_DIR}/TESTS/*/*/*/*/lightning-rpc
    expire_in: 1 week

test-splice-enforcing-socket:
  stage: test
  tags: [ saas-linux-large-amd64 ]
  image: ubuntu
  variables:
    EXPERIMENTAL_SPLICING: "1"
  script:
    - ./scripts/install-additional
    - git submodule update --init --recursive
    - make list-versions
    - ./scripts/setup-remote-hsmd
    - source scripts/setup-env
    - remote_hsmd_socket --git-desc
    - cd lightning
    - unzip -o ../bin-cln.zip
    - bash -x ../scripts/ci-test.sh
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/TESTS/
    exclude:
      - ${CI_PROJECT_DIR}/TESTS/*/*/*/*/lightning-rpc
    expire_in: 1 week

test-dualfund-enforcing-socket:
  stage: test
  tags: [ saas-linux-large-amd64 ]
  image: ubuntu
  variables:
    EXPERIMENTAL_DUAL_FUND: "1"
  script:
    - ./scripts/install-additional
    - git submodule update --init --recursive
    - make list-versions
    - ./scripts/setup-remote-hsmd
    - source scripts/setup-env
    - remote_hsmd_socket --git-desc
    - cd lightning
    - unzip -o ../bin-cln.zip
    - bash -x ../scripts/ci-test.sh
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/TESTS/
    exclude:
      - ${CI_PROJECT_DIR}/TESTS/*/*/*/*/lightning-rpc
    expire_in: 1 week
